// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'QuestionInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$QuestionInfoToJson(QuestionInfo instance) =>
    <String, dynamic>{
      'numberOfQuestion': instance.numberOfQuestion,
      'userAnswers': QuestionInfo.userAnswersToJs(instance.userAnswers),
    };
