// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'QuestionSubmit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Map<String, dynamic> _$QuestionSubmitToJson(QuestionSubmit instance) =>
    <String, dynamic>{
      'questionId': instance.questionId,
      'questionToken': instance.questionToken,
      'answerId': instance.answerId,
    };
